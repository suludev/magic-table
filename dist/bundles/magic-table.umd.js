(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports) :
	typeof define === 'function' && define.amd ? define(['exports'], factory) :
	(factory((global['magic-table'] = {})));
}(this, (function (exports) { 'use strict';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes} checked by tsc
 */
var MagicTable = {};

exports.MagicTable = MagicTable;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=magic-table.umd.js.map
