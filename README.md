# MagicTable

Its a easy material table to use on [Angular.io](http://Angular.io).

This project was made so it was easy to make dynamic material tables without the need of any other thing but Array's.

---
### Import

Import the *MagicTableModule* into the module you want to use it.

``` {...imports: [MagicTableModule...]} ```

---

### Use

Once you have imported the *MagicTableModule* now you can use it like...

*File <app.component.html>*
```html
<table magic-table #mT="magicTable">
  <thead magic-header>
  <tr magic-row>
    <th magic-column="name">Name</th>
    <th magic-column="lastName">Last name</th>
    <th magic-column="value" class="magic-numeric">Value</th>
  </tr>
  </thead>
  <tbody magic-body>
  <tr magic-row *ngFor="let item of items | magicOderBy: mT.data.orderBy, mT.data.dec" (click)="doSomthing(item, $event)">
    <td magic-cell>{{item.name}}</td>
    <td magic-cell>{{item.lastName}}</td>
    <td magic-cell class="magic-numeric">{{item.value}}</td>
  </tr>
  </tbody>
</table>
```
*File <ap.component.ts>*
```javascript
import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public items = [
    {
      name: 'Pineapple',
      lastName: 'aa',
      value: 10022
    },
    {
      name: 'Apple',
      lastName: 'aasdfsadf',
      value: 3449
    },
    {
      name: 'Mango',
      lastName: 'bbb',
      value: 6565
    },
    {
      name: 'Strawberry',
      lastName: 'asdfsadfasdf',
      value: 23555
    },
    {
      name: 'Pear',
      lastName: 'safasdfasdf',
      value: 99474
    },
    {
      name: 'Grape',
      lastName: 'asdfrrrterer',
      value: 773
    },
    {
      name: 'Kiwi',
      lastName: 'ppoasfdposapdf',
      value: 8877
    }
  ];

}
```
---

This project is onther the MIT license, you can see the license on the file <license.md> 
