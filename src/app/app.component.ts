import { Component } from '@angular/core';
import { Sort, MatSortHeaderIntl } from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public data = [
    {
      name: 'Pineapple',
      lastName: 'aa',
      value: 10022
    },
    {
      name: 'Apple',
      lastName: 'aasdfsadf',
      value: 3449
    },
    {
      name: 'Mango',
      lastName: 'bbb',
      value: 6565
    },
    {
      name: 'Strawberry',
      lastName: 'asdfsadfasdf',
      value: 23555
    },
    {
      name: 'Pear',
      lastName: 'safasdfasdf',
      value: 99474
    },
    {
      name: 'Grape',
      lastName: 'asdfrrrterer',
      value: 773
    },
    {
      name: 'Kiwi',
      lastName: 'ppoasfdposapdf',
      value: 8877
    }
  ];

  public sortedData = this.data;

  sortData(sort: Sort) {

    console.log(sort);

  }

}
